import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { useAppContext } from './components/hooks'

const ProtectedRoute = ({ component: Component, ...rest }) => {
  const { state: { isAuth } } = useAppContext()

  return (
    <Route
      render={props =>
        !isAuth ? <Redirect to="/login" /> : <Component {...props} />
      }
      {...rest}
    />
  )
}

export default ProtectedRoute
