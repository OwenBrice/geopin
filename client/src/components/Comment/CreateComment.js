import React from 'react'
import { makeStyles } from '@material-ui/core'
// import InputBase from "@material-ui/core/InputBase";
// import IconButton from "@material-ui/core/IconButton";
// import ClearIcon from "@material-ui/icons/Clear";
// import SendIcon from "@material-ui/icons/Send";
// import Divider from "@material-ui/core/Divider";

const useStyles = makeStyles(theme => ({
  form: {
    display: 'flex',
    alignItems: 'center'
  },
  input: {
    marginLeft: 8,
    flex: 1
  },
  clearButton: {
    padding: 0,
    color: 'red'
  },
  sendButton: {
    padding: 0,
    color: theme.palette.secondary.dark
  }
}))

function CreateComment() {
  const classes = useStyles()

  return <div>CreateComment</div>
}

export default CreateComment
