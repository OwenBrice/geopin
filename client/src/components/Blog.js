import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
// import { Paper } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    minWidth: 350,
    maxWidth: 400,
    maxHeight: 'calc(100vh - 64px)',
    overflowY: 'scroll',
    display: 'flex',
    justifyContent: 'center'
  },
  rootMobile: {
    maxWidth: '100%',
    maxHeight: 300,
    overflowX: 'hidden',
    overflowY: 'scroll'
  }
})

function Blog() {
  const classes = useStyles()

  return <div>Blog</div>
}

export default Blog
