import React from 'react'
import { GoogleLogin } from 'react-google-login'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import { useAppContext } from '../hooks'
import { onSuccessLogin, isloggedIn } from '../../store/actions'
import { ME_QUERY } from '../../graphql/queries'

function Login() {
  const classes = useStyles()
  const { dispatch, client } = useAppContext()

  const onSuccess = async googleUser => {
    try {
      const idToken = googleUser.getAuthResponse().id_token
      localStorage.setItem('token', idToken)
      const {
        data: { me }
      } = await client.query({ query: ME_QUERY })
      dispatch(onSuccessLogin(me))
      dispatch(isloggedIn(googleUser.isSignedIn()))
    } catch (err) {
      onFailure(err)
    }
  }

  const onFailure = err => {
    console.log('Error logging in', err)
  }

  return (
    <div className={classes.root}>
      <Typography
        component="h2"
        variant="h3"
        gutterBottom
        noWrap
        style={{ color: 'rgb(66, 133, 224)' }}
      >
        Welcome
      </Typography>
      <GoogleLogin
        clientId="723272451718-ns7ld0b90rb6gr4h8dc4op1160ik267f.apps.googleusercontent.com"
        onSuccess={onSuccess}
        onFailure={onFailure}
        isSignedIn={true}
        buttonText="Login with google"
        theme="dark"
      />
    </div>
  )
}

const useStyles = makeStyles({
  root: {
    height: '100vh',
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center'
  }
})

export default Login
