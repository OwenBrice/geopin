import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
// import ExitToApp from "@material-ui/icons/ExitToApp";
// import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  root: {
    cursor: 'pointer',
    display: 'flex'
  },
  buttonText: {
    color: 'orange'
  },
  buttonIcon: {
    marginLeft: '5px',
    color: 'orange'
  }
})

function Signout() {
  const classes = useStyles()

  return <div>Signout</div>
}

export default Signout
