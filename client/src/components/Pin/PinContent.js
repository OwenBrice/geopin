import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
// import Typography from "@material-ui/core/Typography";
// import AccessTime from "@material-ui/icons/AccessTime";
// import Face from "@material-ui/icons/Face";

const useStyles = makeStyles(theme => ({
  root: {
    padding: '1em 0.5em',
    textAlign: 'center',
    width: '100%'
  },
  icon: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1)
  },
  text: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
}))

function PinContent() {
  const classes = useStyles()

  return <div>PinContent</div>
}

export default PinContent
