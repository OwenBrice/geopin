import { purple, green, red } from '@material-ui/core/colors'
import { createMuiTheme } from '@material-ui/core/styles'

// A custom theme for this app
const theme = createMuiTheme({
  palette: {
    primary: {
      light: purple[300],
      main: purple[500],
      dark: purple[700]
    },
    secondary: {
      light: green[300],
      main: green[500],
      dark: green[700]
    },
    error: {
      main: red.A400
    },
    background: {
      default: '#fff'
    },
    typography: {
      useNextVariants: true
    }
  }
})

export default theme
