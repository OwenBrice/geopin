import React, { useReducer } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Context from './context'
import reducer, { initialState } from './store/reducer'
import 'mapbox-gl/dist/mapbox-gl.css'
import * as serviceWorker from './serviceWorker'
import { ApolloProvider } from 'react-apollo-hooks'
import client from './clientApollo'
import ProtectedRoute from './ProtectedRoute'
import App from './pages/App'
import Splash from './pages/Splash'

function Root() {
  const [state, dispatch] = useReducer(reducer, initialState)
  console.log('state', state)

  return (
    <Router>
      <ApolloProvider client={client}>
        <Context.Provider value={{ state, dispatch, client }}>
          <Switch>
            <ProtectedRoute exact path="/" component={App} />
            <Route exact path="/login" component={Splash} />
          </Switch>
        </Context.Provider>
      </ApolloProvider>
    </Router>
  )
}

ReactDOM.render(<Root />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
