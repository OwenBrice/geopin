import { LOGIN_USER, IS_LOGGED_IN } from './types'

export const initialState = {
  currentUser: null,
  isAuth: false
}

const reducer = (state, { type, payload }) => {
  switch (type) {
    case LOGIN_USER:
      return {
        ...state,
        currentUser: payload
      }
    case IS_LOGGED_IN:
      return {
        ...state,
        isAuth: payload
      }
    default:
      return state
  }
}

export default reducer
