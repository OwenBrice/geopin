import { LOGIN_USER, IS_LOGGED_IN } from './types'

export const onSuccessLogin = payload => {
  return {
    type: LOGIN_USER,
    payload
  }
}

export const isloggedIn = payload => {
  return {
    type: IS_LOGGED_IN,
    payload
  }
}
