import gql from 'graphql-tag'

export const ME_QUERY = gql`
  query {
    me {
      _id
      name
      email
      picture
    }
  }
`
