import React from 'react'
import { useAppContext } from '../components/hooks'
import { Redirect } from 'react-router-dom'
import Login from '../components/Auth/Login'

function Splash() {
  const {
    state: { isAuth }
  } = useAppContext()

  return isAuth ? <Redirect to="/" /> : <Login />
}

export default Splash
