import mongoose, { Schema } from 'mongoose'

const UserSchema = Schema({
  name: String,
  email: String,
  picture: String
})

export default mongoose.model('User', UserSchema)
