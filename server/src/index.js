const { ApolloServer } = require('apollo-server')
import mongoose from 'mongoose'
import dotenv from 'dotenv'
import typeDefs from './typeDefs'
import { Query } from './resolvers'
import { findOrCreateUser } from './controllers/userController'

dotenv.config()

mongoose
  .connect(process.env.MONGO_URI, { useNewUrlParser: true })
  .then(() => console.log('DB connected'))
  .catch(() => console.error(err))

const server = new ApolloServer({
  typeDefs,
  resolvers: {
    Query
  },
  context: async ({ req }) => {
    let authToken = null
    let currentUser = null

    try {
      authToken = req.headers.authorization
      if (authToken) {
        // Find or create user
        currentUser = await findOrCreateUser(authToken)
      }
    } catch (err) {
      console.error(`Unable to  authenticate user with token ${authToken}`)
    }

    return { currentUser }
  }
})

server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`)
})
