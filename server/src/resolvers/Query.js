import { AuthenticationError } from 'apollo-server'

const authenticatd = next => (root, args, ctx, info) => {
  if (!ctx.currentUser) {
    throw new AuthenticationError('You must be logged in')
  }

  return next(root, args, ctx, info)
}

const Query = {
  me: authenticatd((root, args, ctx, info) => ctx.currentUser)
}

export default Query
